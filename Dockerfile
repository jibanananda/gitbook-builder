FROM node:10
COPY package_install package_dist_upgrade /usr/sbin/
RUN chmod 755 /usr/sbin/package_install /usr/sbin/package_dist_upgrade && \
    package_dist_upgrade && \
    package_install calibre && \
    npm install gitbook-cli -g && \
    gitbook fetch 3.2.3
